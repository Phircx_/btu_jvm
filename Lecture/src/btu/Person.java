package btu;


public class Person {
    String name = "Anna";
    String lastname;
    int age;

    public void setName(String name)
    {
        this.name = name;
    }

//    უპარამეტრო
    public Person(String name) {
        this.name = name;
    }

    public static void main(String[] args){
        Person myObj = new Person("Luka");
        System.out.println(myObj);
    }

}
