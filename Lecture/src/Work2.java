
import java.util.Arrays;
import java.util.Scanner;

public class Work2 {
    public static void main(String[] args) {
        System.out.println("ამოცანა #1");

        String[] strAr1 = new String[]{"C++","C#","java","pascal","php", "JavaScript","ActionScript"};
        for (int i=0;i<=strAr1.length-1;i++){
            System.out.println(strAr1[i]);
        }

        System.out.println("ამოცანა #2");

        int x,y,divide,root;
        Scanner sc = new Scanner(System.in);
        System.out.print("შეიყვანეთ პირველი რიცხვი: ");
        x=sc.nextInt();
        System.out.print("შეიყვანეთ მეორე რიცხვი: ");
        y=sc.nextInt();
        divide=divide(x,y);
        root=root(x,y);
        System.out.println("პირველის მეორეზე გაყოფისას მიღებული რიცხვი ან ციფრია " + divide);
        System.out.println("მეორე რიცხვის პირველ რიცხვზე გაყოფისას მიღებული ნაშთია " + root);


        System.out.println("ამოცანა #3");

        int sumOfThree,multiply,z;
        System.out.print("შეიყვანეთ სამი ციფრი: ");
        x=sc.nextInt();
        y=sc.nextInt();
        z=sc.nextInt();
        sumOfThree=sumOfThree(x,y,z);
        multiply=multiply(x,y,z);
        System.out.println("შეყვანილი რიცხვების ჯამი "+sumOfThree+"ს ტოლია, ხოლო მათი ნამრავლი "+multiply+"ს");

        System.out.println("ამოცანა #4");

        System.out.println("შეიყვანეთ სამნიშნა რიცხვი");
        x=sc.nextInt();
        char[] result = String.valueOf(x).toCharArray();
        for (char i=0;i<=result.length-1;i++){
            System.out.println(result[i]);
        }

        System.out.println("ამოცანა #5/6 ორივე პირობას აკმაყოფილებს");
        System.out.println("შეიყვანეთ ოთხნიშნა რიცხვი");
        y=sc.nextInt();
        int sum = 0;
        char[] numbArray = String.valueOf(y).toCharArray();
        for(char k=0;k<=numbArray.length-1;k++){
            sum += Integer.parseInt(String.valueOf(numbArray[k]));

        }
        System.out.println(sum);


        System.out.println("ამოცანა #7");

        System.out.println("შეიყვანეთ პირველი რიცხვი");
        x= sc.nextInt();
        System.out.println("შეიყვანეთ მეორე რიცხვი");
        y= sc.nextInt();
        int gcd = gcd(x,y);
        int gcdVal;
        gcdVal=gcd(x,y);
        System.out.println("უდიდესი საერთო გამყოფია"+ gcd);
        System.out.println("უმცირესი საერთო ჯერადია"+ lcm(x,y,gcdVal));

        System.out.println("ამოცანა #8");

        int m,n;
        System. out.println("შეიტანეთ პირველი რიცხვი (M): ");
        m = sc.nextInt();
        System.out.println("შეიტანეთ მეორე რიცხვი (N): ");
        n = sc.nextInt();
        if(m<=n){
            for (int i=m;i<=n-1;i++){
                System.out.println(i);
            }
        }

        System.out.println("ამოცანა #9");

        int array[] =  new int[]{10,15,46,25,68,1,15,87};
        int smallest = array[0];
        int theBiggest = array[0];
        for (int i=1;i<array.length;i++){
            if (array[i]> theBiggest){
                theBiggest = array[i];
            }else if (array[i]<smallest){
                smallest = array[0];
            }
        }
        System.out.println("მასივის უდიდესი რიცხვია" + theBiggest);
        System.out.println("მასივის უმცირესი რიცხვია " + smallest);


        System.out.println("ამოცანა #10");

        int array2[] =  new int[]{17,15,55,684,235,1655,2550,31};
        Arrays.sort(array2);
        for (int i=0;i<array2.length;i++){
            System.out.print(array2[i]+" ");
        }

        System.out.println("ამოცანა #11");

        int array3[] = new int[8];
        System.out.println("მასივში ჩაწერილი შემთხვევითი(Random) რიცხვები");
        for (int i=0;i<=array3.length-1;i++){
            array3[i]=(int) (Math.random() * 100);
            System.out.print(array3[i] + " ");
        }


        System.out.println("ამოცანა #12");

        int array4[] = new int [8];
        for (int i=0;i<array4.length-1;i++) {
            array4[i] = (int) (Math.random() * 1000);
        }
        Arrays.sort(array4);
        System.out.println("მასივში ჩაწერილი Random მთელი რიცხვები,დალაგებული კლებადობით");
        for (int k=array4.length-1;k>=0;k--){
            System.out.print(array4[k] + " ");
        }
    }

    public static int divide(int first, int second){
        int divide = first / second;
        return divide;
    }
    public static int root(int first,int second) {
        int root = second%first;
        return root;
    }
    public static int sumOfThree(int first,int second,int third){
        int sumOfThree = first + second + third;
        return sumOfThree;
    }
    public static int multiply(int first,int second,int third) {
        int multiply = first*second*third;
        return multiply;
    }

    public static int gcd(int a, int b){
        if (b == 0){
            return a;
        }else {
            return gcd(b,a%b);
        }

    }
    public static int lcm(int a, int b, int gcdVal){
        return Math.abs(a*b) /gcdVal;
    }
}
