package Work3;
import java.util.Scanner;

public class Project2 {
    public static class A {
        public static Scanner sc = new Scanner(System.in);
        int[] m= new int[10];

        public void method1() {
            for (int i=0;i<m.length-1;i++){
                m[i]=sc.nextInt();
            }
        }

        public void method2() {
            for (int i=0;i<m.length-1;i++){
                m[i]=sc.nextInt();
                m[i] = (int) (Math.random() * 15);
            }
        }

        public int method3(){
            int sum = 0;
            for (int i=0;i<m.length-1;i++){
                if (m[i]< i){
                    sum += m[i];
                }
            }
            return sum;
        }

        public int method4() {
            int multiply = 1;
            for (int i=0;i<m.length-1;i++){
                if (m[i]> i){
                    multiply += m[i];
                }
            }
            return multiply;
        }

    }
    public static void main(String[] args) {
        A obj1 = new A();

    }
}
