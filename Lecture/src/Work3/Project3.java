package Work3;

public class Project3 {
    public static class A {
        private int x;

        public A(int x){
            System.out.println("Hello My Class");
            this.x = x;
        }

        public boolean method1() {
            return x%2==0;
        }

    }

    public static void main(String[] args) {
        A obj1 = new A(10);
        System.out.println(obj1.method1());
    }
}
