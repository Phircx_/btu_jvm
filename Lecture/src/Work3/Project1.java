package Work3;
import java.util.Scanner;
import java.util.Arrays;

public class Project1 {
    public static class A {
        public static Scanner sc = new Scanner(System.in);
        private int a,b;

        public void method1(){
            a=sc.nextInt();
            b=sc.nextInt();
        }
        public void method2() {
            System.out.println(a+b);
        }
        public int method3(){
            return a*b;
        }
    }



    public static class B{
        private int a,b,c;

        public void method1(){
            a=A.sc.nextInt();
            b=A.sc.nextInt();
            c=A.sc.nextInt();
        }

        public void method2(){
            if (a>b && a>c) {
                System.out.println(a);
            }else if(b>a&& b>c) {
                System.out.println(b);
            }else if (c>a && c>b) {
                System.out.println(c);
            }
        }

        public int method3() {
            if(a<b && a<c) {
                return a;
            }else if(b<a && b<c){
                return b;
            }else if(c<a && c<b ){
                return c;
            }else {
                return a;
            }
        }
    }
    public static class C {
        private int a,b,c;

        public void method1() {
            a=A.sc.nextInt();
            b=A.sc.nextInt();
            c=A.sc.nextInt();
        }

        public int method2() {
            char[] arrA = Integer.toString(a).toCharArray();
            return arrA[arrA.length-1];
        }

        public int method3() {
            char[] arrB = Integer.toString(b).toCharArray();
            return arrB[arrB[0]];
        }

        public int method4() {
            int result = 0;
            char[] arrC = Integer.toString(c).toCharArray();
            for (char value : arrC) {
                result += Integer.parseInt(String.valueOf(value));
            }
            return result;

        }

        public void method5(){
            System.out.println(method2()*method3());
        }

        public void method6() {
            System.out.println(method3()+method4());
        }

    }

    public static void main(String[] args) {
        A classA = new A();
        classA.method1();
        classA.method2();
        System.out.println(classA.method3());

        B classB = new B();
        classB.method1();
        classB.method2();
        System.out.println(classB.method3());

        C classC = new C();
        classC.method1();
        classC.method2();
        classC.method3();
        System.out.println(classC.method4());
        classC.method5();
        classC.method6();
    }
}
